#BIENVENIDOS!
-------------
En este repositorio estaremos trabajando las maquetas que se vayan solicitando, por lo que es importante entender el flujo de trabajo que estaremos usando.

Todo se guardara dentro del directorio ```maquetas```, el cual contendra cada grupo de maquetas con la fecha en la que estas fueron asignadas _dd/mm/aaaa_.

La estructura sería algo así. 

*	Maquetas.
	*	Fecha.
		*	Nombre_Escritos	
			1.	Nombre_Escritor_Idioma(_EN,ES..._).
			2.	Nombre_Escritor_EPUB_Idioma(_EN,ES..._).
			3.	Figure_1_Idioma(_EN,ES..._).

De esta manera organizamos cada una de las entregas, el css, puede ser enlazado al global, pero en caso del EPUB, debe ser importado a la hoja de estilo así mismo con sus imagenes, de esta manera se podrá visualizar de manera correcta todo,

**REVISAR ANTES DE ENVIAR**
